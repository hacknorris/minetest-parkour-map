# minetest parkour map

so yes. a minetest map for challenging you.

##features:

- have 6 diferent skill levels (very very easy, very easy, easy, medium, medium hard and hard)

- doesnt require any addons (only core minetest)

- can be played both in night and day minetest hours (lights, challenge will be still visible. arent stars at top cool ?)

- can be hosted/selfhosted to challenge other users =] 

- works in creative (non-damage) mode - just remember to come back to checkpoint after failing ;)

##bugs? 

- untested start of game (but should start before first level. wasnt customized)

##how to install? 
just clone/download this repo to your worlds folder in minetest 

(in linux ~/.var/app/net.minetest.Minetest/.minetest/worlds/\* 
on android /storage/emulated/0/minetest/worlds on windows and mac untested),

then open/reopen minetest and you will see it in main menu =]
